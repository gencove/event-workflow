import sys
import json
import logging
from collections import OrderedDict as od

import boto3

def workflow_config(activity_run_jobs_arn):
    # Using OrderedDict to preserve JSON order and human-readability
    return od(
        Comment='Workflow SFN',
        StartAt='RunJob',
        States=od(
            RunJob=od(
                Type='Task',
                Resource=activity_run_jobs_arn,
                ResultPath='$.status',
                Next='Done'
            ),
            Done=od(
                Type='Pass',
                End=True
            )
        )
    )

def poll_activity_config(
        lambda_poll_iterator_arn, lambda_poll_activity_arn, 
        lambda_poll_restart_arn):
    # Using OrderedDict to preserve JSON order and human-readability
    return od(
        Comment='Continuously poll activity',
        StartAt='ConfigureCount',
        States=od(
            ConfigureCount=od(
                Type='Pass',
                Result=od(
                    count=1000,
                    index=-1,
                    step=1
                ),
                ResultPath='$.iterator',
                Next='Iterator'
            ),
            Iterator=od(
                Type='Task',
                Resource=lambda_poll_iterator_arn,
                ResultPath='$.iterator',
                Next='IsCountReached',
                Retry=[
                    od(
                        ErrorEquals=[
                            'Lambda.SdkClientException',
                            'Lambda.ServiceException',
                            'Lambda.Unknown',
                            'Lambda.AWSLambdaException',
                            'States.TaskFailed'
                        ],
                        IntervalSeconds=5,
                        MaxAttempts=6,
                        BackoffRate=2.0
                    )
                ]
            ),
            IsCountReached=od(
                Type='Choice',
                Choices=[
                    od(
                        Variable='$.iterator.continue',
                        BooleanEquals=True,
                        Next='Work'
                    )
                ],
                Default='Restart'
            ),
            Work=od(
                Type='Task',
                Resource=lambda_poll_activity_arn,
                ResultPath='$.result',
                Next='Iterator',
                Retry=[
                    od(
                        ErrorEquals=[
                            'Lambda.SdkClientException',
                            'Lambda.ServiceException',
                            'Lambda.Unknown',
                            'Lambda.AWSLambdaException',
                            'States.TaskFailed'
                        ],
                        IntervalSeconds=5,
                        MaxAttempts=6,
                        BackoffRate=2.0
                    )
                ]
            ),
            Restart=od(
                Type='Task',
                Resource=lambda_poll_restart_arn,
                Next='Done',
                Retry=[
                    od(
                        ErrorEquals=[
                            'Lambda.SdkClientException',
                            'Lambda.ServiceException',
                            'Lambda.Unknown',
                            'Lambda.AWSLambdaException',
                            'States.TaskFailed'
                        ],
                        IntervalSeconds=5,
                        MaxAttempts=6,
                        BackoffRate=2.0
                    )
                ]
            ),
            Done=od(
                Type='Pass',
                End=True 
            )
        )
    )

def step_functions_update(
        sfn_workflow_arn, sfn_poll_activity_arn,
        activity_run_jobs_arn, lambda_poll_iterator_arn, 
        lambda_poll_activity_arn, lambda_poll_restart_arn):
    sfn = boto3.client('stepfunctions')

    logging.info('Updating workflow sfn')
    config1 = workflow_config(activity_run_jobs_arn)
    sfn.update_state_machine(
        stateMachineArn=sfn_workflow_arn,
        definition=json.dumps(config1, indent=4)
    )

    logging.info('Updating activity poller sfn')
    config2 = poll_activity_config(
        lambda_poll_iterator_arn, lambda_poll_activity_arn, 
        lambda_poll_restart_arn)
    sfn.update_state_machine(
        stateMachineArn=sfn_poll_activity_arn,
        definition=json.dumps(config2, indent=4)
    )

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    step_functions_update(
        sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], 
        sys.argv[6])
