import logging
import os

import common

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def poll_restart(event):
    sfn_poll_activity_arn = os.environ['SFN_ACTIVITY']

    sfn = common.thread_safe_client('stepfunctions')

    response = sfn.start_execution(
        stateMachineArn=sfn_poll_activity_arn
    )
    return response['executionArn']

def handler(event, context):
    return poll_restart(event)
