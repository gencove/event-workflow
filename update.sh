#!/bin/bash

set -eo pipefail

# Get data from Terraform
LAMBDA_JOB_DATA_ARN="$(terraform output lambda_job_data_arn)" &&\
LAMBDA_POLL_ACTIVITY_ARN="$(terraform output lambda_poll_activity_arn)" &&\
LAMBDA_POLL_ITERATOR_ARN="$(terraform output lambda_poll_iterator_arn)" &&\
LAMBDA_POLL_RESTART_ARN="$(terraform output lambda_poll_restart_arn)" &&\
SFN_WORKFLOW_ARN="$(terraform output sfn_workflow_arn)" &&\
SFN_POLL_ACTIVITY_ARN="$(terraform output sfn_poll_activity_arn)" &&\
ACTIVITY_RUN_JOBS_ARN="$(terraform output sfn_activity_run_jobs_arn)" &&\
DYNAMODB_TABLE_NAME="$(terraform output dynamodb_job_data_table_id)" &&\
BATCH_QUEUE_ARN="$(terraform output batch_job_queue_high_priority_arn)" &&\
BATCH_JOB_DEFINITION_ARN="$(terraform output batch_job_definition_base_arn)" &&\

echo "Generating AWS Lambda package for environment $ENV" &&\
WORK_DIR=$(mktemp -d) &&\
echo "WORK_DIR: $WORK_DIR" &&\
UPLOAD_FILE="aws_lambda.zip" &&\
echo "Working directory:" &&\
echo "$WORK_DIR" &&\
echo "Copying upload package contents" &&\
cp -r code/lmbd/ $WORK_DIR &&\
echo "Compressing upload package" &&\
pushd $WORK_DIR &&\
zip -q -9 -r $UPLOAD_FILE . &&\
echo "Updating Lambda code" &&\
aws lambda update-function-code --zip-file fileb://$UPLOAD_FILE --function-name $LAMBDA_JOB_DATA_ARN &&\
aws lambda update-function-code --zip-file fileb://$UPLOAD_FILE --function-name $LAMBDA_POLL_ACTIVITY_ARN &&\
aws lambda update-function-code --zip-file fileb://$UPLOAD_FILE --function-name $LAMBDA_POLL_ITERATOR_ARN &&\
aws lambda update-function-code --zip-file fileb://$UPLOAD_FILE --function-name $LAMBDA_POLL_RESTART_ARN &&\
popd &&\
echo "Cleaning up" &&\
rm -rf $WORK_DIR &&\
echo "Updating Lambda variables" &&\
aws lambda update-function-configuration --function-name $LAMBDA_JOB_DATA_ARN --environment "Variables={DYNAMODB_TABLE=$DYNAMODB_TABLE_NAME}" &&\
aws lambda update-function-configuration --function-name $LAMBDA_POLL_ACTIVITY_ARN --environment "Variables={DYNAMODB_TABLE=$DYNAMODB_TABLE_NAME,SFN_ACTIVITY=$ACTIVITY_RUN_JOBS_ARN,BATCH_QUEUE=$BATCH_QUEUE_ARN,BATCH_JOB_DEFINITION=$BATCH_JOB_DEFINITION_ARN}" &&\
aws lambda update-function-configuration --function-name $LAMBDA_POLL_RESTART_ARN --environment "Variables={SFN_ACTIVITY=$ACTIVITY_RUN_JOBS_ARN}" &&\
echo "Updating SFNs" &&\
python3 code/sfn/update.py $SFN_WORKFLOW_ARN $SFN_POLL_ACTIVITY_ARN $ACTIVITY_RUN_JOBS_ARN $LAMBDA_POLL_ITERATOR_ARN $LAMBDA_POLL_ACTIVITY_ARN $LAMBDA_POLL_RESTART_ARN &&\
echo "Done"
