variable "project" {}
variable "env" {}

locals {
  bucket_main_name = "${var.project}-${var.env}"
}

resource "aws_s3_bucket" "main" {
  bucket = "${local.bucket_main_name}"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags {
    Bucket    = "${local.bucket_main_name}"
    Name      = "${local.bucket_main_name}"
    Project   = "${var.project}"
    Env       = "${var.env}"
    ManagedBy = "terraform"
  }
}

## Output

output s3_bucket_main_arn {
  value = "${aws_s3_bucket.main.arn}"
}

output s3_bucket_main_id {
  value = "${aws_s3_bucket.main.id}"
}
