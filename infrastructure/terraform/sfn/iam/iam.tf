variable "project" {}
variable "env" {}
variable "name" {}

locals {
  name_full = "${var.project}-${var.env}-${var.name}"
}

data "aws_region" "current" {}

data "aws_iam_policy_document" "lambda_sfn_execution_policy_document" {
  statement {
    sid = "LambdaPolicy"

    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "lambda_sfn_execution_policy" {
  name = "${local.name_full}-lambda_sfn_execution_policy"
  path = "/${var.project}/${var.env}/sfn/${var.name}/"
  policy = "${data.aws_iam_policy_document.lambda_sfn_execution_policy_document.json}"
}

data "aws_iam_policy_document" "lambda_sfn_execution_role_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["states.${data.aws_region.current.name}.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "lambda_sfn_execution_role" {
  name = "${local.name_full}-lambda_sfn_execution_role"
  path = "/${var.project}/${var.env}/sfn/${var.name}/"

  assume_role_policy = "${data.aws_iam_policy_document.lambda_sfn_execution_role_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_sfn_execution_role_1" {
  role = "${aws_iam_role.lambda_sfn_execution_role.id}"
  policy_arn = "${aws_iam_policy.lambda_sfn_execution_policy.arn}"
}

output sfn_role_arn {
  value = "${aws_iam_role.lambda_sfn_execution_role.arn}"
}
