variable "project" {}
variable "env" {}
variable "name" {}

variable "lambda_role_arn" {}
variable "dlq_arn" {}

variable "memory_size" {
  default = 256
}
variable "timeout" {
  default = 210
}

locals {
  lambda_name = "${var.project}-${var.env}-${var.name}"
}

resource "aws_lambda_function" "lambda" {
  function_name    = "${local.lambda_name}"
  filename         = "${path.root}/lambda/main.zip"
  role             = "${var.lambda_role_arn}"
  handler          = "${var.name}.handler"
  runtime          = "python3.6"
  memory_size      = "${var.memory_size}"
  timeout          = "${var.timeout}"

  dead_letter_config = {
    target_arn = "${var.dlq_arn}"
  }

  # Manage code and variables via builds
  lifecycle {
    ignore_changes = [
      "environment",
      "filename",
      "s3_bucket",
      "s3_key",
      "s3_object_version",
      "source_code_hash"
    ]
  }
}

output lambda_arn {
  value = "${aws_lambda_function.lambda.arn}"
}

output lambda_function_name {
  value = "${aws_lambda_function.lambda.function_name}"
}
