locals {
  region = "us-east-1"
}

provider "aws" {
  region = "${local.region}"
}

resource "random_string" "uniq" {
  length  = 6
  upper   = false
  lower   = true
  number  = true
  special = false
  keepers = {
    # Update this to generate new password
    uniq_version = "1"
  }
}

locals {
  project = "workflow-${random_string.uniq.result}"
  env = "testing"
  # Basic Amazon ECS-Optimized AMI in us-east-1
  ami_id = {
    us-east-1 = "ami-112e366e"
  }
}

## DynamoDB
#
module "dynamodb_job_data" {
  source  = "dynamodb"
  project = "${local.project}"
  env = "${local.env}"
  table_name = "job_data"
}

## VPC

module "vpc" {
  source  = "vpc"
  project = "${local.project}"
  env = "${local.env}"
  ver = "1"
}

## Batch

module "batch" {
  source  = "batch"
  project = "${local.project}"
  env = "${local.env}"
  ver = "1"
  subnet_ids = "${module.vpc.subnet_ids}"
  security_group_ids = "${module.vpc.compute_environment_security_group_ids}"
  image_id = "${local.ami_id[local.region]}"
  s3_main_arn = "${module.s3.s3_bucket_main_arn}"
  lambda_job_data_arn = "${module.lambda_job_data.lambda_arn}"
}

## S3

module "s3" {
  source = "s3"
  project = "${local.project}"
  env = "${local.env}"
}

## Step functions

module "sfn_iam" {
  source = "sfn/iam"
  project = "${local.project}"
  env = "${local.env}"
  name = "main"
}

module "sfn_workflow" {
  source = "sfn"
  project = "${local.project}"
  env = "${local.env}"
  name = "workflow"
  sfn_role_arn = "${module.sfn_iam.sfn_role_arn}"
}

resource "aws_sfn_activity" "run_jobs" {
  name = "${local.project}-${local.env}-run_jobs"
}

module "sfn_poll_activity" {
  source = "sfn"
  project = "${local.project}"
  env = "${local.env}"
  name = "poll_activity"
  sfn_role_arn = "${module.sfn_iam.sfn_role_arn}"
}

module "sfn_alarms_workflow" {
  source = "sfn/alarms"
  sfn_name = "${module.sfn_workflow.sfn_name}"
  sfn_arn = "${module.sfn_workflow.sfn_arn}"
  sns_arn = "${aws_sns_topic.dlq.arn}"
}

module "sfn_alarms_poll_activity" {
  source = "sfn/alarms"
  sfn_name = "${module.sfn_poll_activity.sfn_name}"
  sfn_arn = "${module.sfn_poll_activity.sfn_arn}"
  sns_arn = "${aws_sns_topic.dlq.arn}"
}

## SNS topics

resource "aws_sns_topic" "dlq" {
  name = "${local.project}-${local.env}-dlq"
}

## Lambdas
# 
# Add new lambdas to output

module "lambda_iam" {
  source = "lambda/iam"
  project = "${local.project}"
  env = "${local.env}"
  name = "main"
  sns_topic_dlq_arn = "${aws_sns_topic.dlq.arn}"
  s3_main_arn = "${module.s3.s3_bucket_main_arn}"
  sfn_workflow_arn = "${module.sfn_workflow.sfn_arn}"
  sfn_poll_activity_arn = "${module.sfn_poll_activity.sfn_arn}"
  activity_run_jobs_arn = "${aws_sfn_activity.run_jobs.id}"
  dynamodb_job_data_arn = "${module.dynamodb_job_data.table_arn}"
}
module "lambda_poll_activity" {
  source = "lambda/no_vpc"
  project = "${local.project}"
  env = "${local.env}"
  name = "poll_activity"
  lambda_role_arn = "${module.lambda_iam.lambda_role_arn}"
  dlq_arn = "${aws_sns_topic.dlq.arn}"
}
module "lambda_poll_iterator" {
  source = "lambda/no_vpc"
  project = "${local.project}"
  env = "${local.env}"
  name = "poll_iterator"
  lambda_role_arn = "${module.lambda_iam.lambda_role_arn}"
  dlq_arn = "${aws_sns_topic.dlq.arn}"
}
module "lambda_poll_restart" {
  source = "lambda/no_vpc"
  project = "${local.project}"
  env = "${local.env}"
  name = "poll_restart"
  lambda_role_arn = "${module.lambda_iam.lambda_role_arn}"
  dlq_arn = "${aws_sns_topic.dlq.arn}"
}
module "lambda_job_data" {
  source = "lambda/no_vpc"
  project = "${local.project}"
  env = "${local.env}"
  name = "job_data"
  lambda_role_arn = "${module.lambda_iam.lambda_role_arn}"
  dlq_arn = "${aws_sns_topic.dlq.arn}"
  memory_size = 384
}

## Outputs

output "project" {
  value = "${local.project}"
}

output "env" {
  value = "${local.env}"
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "batch_compute_environment_arn" {
  value = "${module.batch.batch_compute_environment_arn}"
}

output "batch_job_queue_high_priority_arn" {
  value = "${module.batch.batch_job_queue_high_priority_arn}"
}

output "batch_job_queue_low_priority_arn" {
  value = "${module.batch.batch_job_queue_low_priority_arn}"
}

output "batch_iam_ecs_task_role_arn" {
  value = "${module.batch.batch_iam_ecs_task_role_arn}"
}

output "batch_job_definition_base_arn" {
  value = "${module.batch.batch_job_definition_base_arn}"
}

output "sns_topic_dlq_arn" {
  value = "${aws_sns_topic.dlq.arn}"
}

# S3

output "s3_bucket_main_arn" {
  value = "${module.s3.s3_bucket_main_arn}"
}

output "s3_bucket_main_id" {
  value = "${module.s3.s3_bucket_main_id}"
}

# SFN

output "sfn_workflow_arn" {
  value = "${module.sfn_workflow.sfn_arn}"
}

output "sfn_poll_activity_arn" {
  value = "${module.sfn_poll_activity.sfn_arn}"
}

output "sfn_activity_run_jobs_arn" {
  value = "${aws_sfn_activity.run_jobs.id}"
}

# LAMBDA

# POLL
output "lambda_poll_activity_arn" {
  value = "${module.lambda_poll_activity.lambda_arn}"
}
output "lambda_poll_activity_function_name" {
  value = "${module.lambda_poll_activity.lambda_function_name}"
}
output "lambda_poll_iterator_arn" {
  value = "${module.lambda_poll_iterator.lambda_arn}"
}
output "lambda_poll_iterator_function_name" {
  value = "${module.lambda_poll_iterator.lambda_function_name}"
}
output "lambda_poll_restart_arn" {
  value = "${module.lambda_poll_restart.lambda_arn}"
}
output "lambda_poll_restart_function_name" {
  value = "${module.lambda_poll_restart.lambda_function_name}"
}

# JOB DATA
output "lambda_job_data_arn" {
  value = "${module.lambda_job_data.lambda_arn}"
}
output "lambda_job_data_function_name" {
  value = "${module.lambda_job_data.lambda_function_name}"
}

# DYNAMODB

output "dynamodb_job_data_table_arn" {
  value = "${module.dynamodb_job_data.table_arn}"
}

output "dynamodb_job_data_table_id" {
  value = "${module.dynamodb_job_data.table_id}"
}

# REGION

output "aws_region" {
  value = "${local.region}"
}
