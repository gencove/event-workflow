variable "project" {}
variable "env" {}
variable "ver" {}

locals {
  vpc_name                                = "${var.project}-${var.env}-${var.ver}"
  compute_environment_security_group_name = "${local.vpc_name}-allow_ssh_in"
}

# Get current region
#
data "aws_region" "current" {}

# Get availability zones in current region
#
data "aws_availability_zones" "all" {}

variable "region_number" {
  # Arbitrary mapping of region name to number to use in
  # a VPC's CIDR prefix.
  default = {
    us-east-1      = 1
    us-east-2      = 2
    us-west-1      = 3
    us-west-2      = 4
    ca-central-1   = 5
    eu-central-1   = 6
    eu-west-1      = 7
    eu-west-2      = 8
    eu-west-3      = 9
    ap-northeast-1 = 10
    ap-northeast-2 = 11
    ap-northeast-3 = 12
    ap-southeast-1 = 13
    ap-southeast-2 = 14
    ap-south-1     = 15
    sa-east-1      = 16
  }
}

variable "az_number" {
  # Assign a number to each AZ letter used in our configuration
  default = {
    a = 1
    b = 2
    c = 3
    d = 4
    e = 5
    f = 6
    g = 7
    h = 8
    i = 9
    j = 10
    k = 11
    l = 12
    m = 13
    n = 14
    o = 15
  }
}

# Retrieve details for all AZs where we want to create network resources
# This must be in the region selected on the AWS provider.
data "aws_availability_zone" "compute_environment" {
  count = "${length(data.aws_availability_zones.all.names)}"

  name  = "${element(data.aws_availability_zones.all.names, count.index)}"
}

resource "aws_vpc" "compute_environment" {
  cidr_block           = "${cidrsubnet("10.0.0.0/8", 8, var.region_number[data.aws_region.current.name])}"
  enable_dns_hostnames = true

  tags {
    Vpc       = "${local.vpc_name}"
    Name      = "${local.vpc_name}"
    Project   = "${var.project}"
    Env       = "${var.env}"
    Version   = "${var.ver}"
    ManagedBy = "terraform"
  }
}

resource "aws_security_group" "compute_environment" {
  name        = "${local.compute_environment_security_group_name}"
  description = "Allow inbound SSH traffic"
  vpc_id      = "${aws_vpc.compute_environment.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    SecurityGroup = "${local.compute_environment_security_group_name}"
    Name          = "${local.compute_environment_security_group_name}"
    Project       = "${var.project}"
    Env           = "${var.env}"
    Version       = "${var.ver}"
    ManagedBy     = "terraform"
  }
}

resource "aws_subnet" "compute_environment" {
  count = "${length(data.aws_availability_zones.all.names)}"

  vpc_id = "${aws_vpc.compute_environment.id}"
  cidr_block = "${cidrsubnet(aws_vpc.compute_environment.cidr_block, 4, var.az_number[element(data.aws_availability_zone.compute_environment.*.name_suffix, count.index)])}"
  map_public_ip_on_launch = true
  availability_zone = "${element(data.aws_availability_zone.compute_environment.*.name, count.index)}"

  tags {
    Subnet    = "${local.vpc_name}-${element(data.aws_availability_zone.compute_environment.*.name, count.index)}"
    Name      = "${local.vpc_name}-${element(data.aws_availability_zone.compute_environment.*.name, count.index)}"
    Project   = "${var.project}"
    Env       = "${var.env}"
    Version   = "${var.ver}"
    ManagedBy = "terraform"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.compute_environment.id}"

  tags {
    Gateway   = "main"
    Name      = "${local.vpc_name}"
    Project   = "${var.project}"
    Env       = "${var.env}"
    Version   = "${var.ver}"
    ManagedBy = "terraform"
  }
}

resource "aws_route" "gw_route" {
  route_table_id            = "${aws_vpc.compute_environment.default_route_table_id}"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = "${aws_internet_gateway.gw.id}"
}

locals {
  compute_environment_security_group_ids = [
    "${aws_security_group.compute_environment.id}"
  ]
}

output vpc_id {
  value = "${aws_vpc.compute_environment.id}"
}

output compute_environment_security_group_ids {
  value = "${join(",", local.compute_environment_security_group_ids)}"
}

output subnet_ids {
  value = "${join(",", aws_subnet.compute_environment.*.id)}"
}

output default_route_table_id {
  value = "${aws_vpc.compute_environment.default_route_table_id}"
}