variable subnet_ids {}
variable security_group_ids {}
variable "bid_percentage" {
  default = 60
}

variable "image_id" {}
variable "min_vcpus" {
  default = 0
}
variable "desired_vcpus" {
  default = 0
}
variable "max_vcpus" {
  default = 32
}

resource "aws_batch_compute_environment" "compute_environment" {
  compute_environment_name = "${local.cluster_name}"

  service_role = "${aws_iam_role.aws_batch_service_role.arn}"
  type = "MANAGED"
  state = "ENABLED"

  compute_resources {
    bid_percentage = "${var.bid_percentage}"
    image_id = "${var.image_id}"
    instance_role = "${aws_iam_instance_profile.ecs_instance_profile.arn}"
    instance_type = ["optimal"]
    min_vcpus = "${var.min_vcpus}"
    desired_vcpus = "${var.desired_vcpus}"
    max_vcpus = "${var.max_vcpus}"
    spot_iam_fleet_role = "${aws_iam_role.spot_fleet_role.arn}"
    type = "SPOT"
    security_group_ids = ["${split(",", var.security_group_ids)}"]
    subnets = ["${split(",", var.subnet_ids)}"]
    tags {
      Name               = "${local.cluster_name}"
      ComputeEnvironment = "${local.cluster_name}"
      Project            = "${var.project}"
      Env                = "${var.env}"
      Version            = "${var.ver}"
      ManagedBy          = "terraform"
    }
  }

  # Prevent a race condition during environment deletion. Needed only for
  # compute-environment-level roles. For details see:
  # https://www.terraform.io/docs/providers/aws/r/batch_compute_environment.html
  #
  depends_on = [
    "aws_iam_role_policy_attachment.aws_batch_service_role_1"
  ]

  lifecycle {
    ignore_changes = [
      # This automatically changes throughout the lifecycle of the environment,
      # so ignore it
      "compute_resources.0.desired_vcpus"
    ]
  }
}

resource "aws_batch_job_queue" "high_priority" {
  name = "${local.cluster_name}-high"
  state = "ENABLED"
  priority = 1000
  compute_environments = ["${aws_batch_compute_environment.compute_environment.arn}"]
}

resource "aws_batch_job_queue" "low_priority" {
  name = "${local.cluster_name}-low"
  state = "ENABLED"
  priority = 10
  compute_environments = ["${aws_batch_compute_environment.compute_environment.arn}"]
}

resource "aws_batch_job_definition" "base" {
    name = "${local.cluster_name}-base"
    type = "container"
    lifecycle {
      ignore_changes = [
        "retry_strategy",
        "container_properties",
        "parameters"
      ]
    }
    retry_strategy {
      attempts = 2
    }
    container_properties = <<CONTAINER_PROPERTIES
{
    "command": ["ls", "-la"],
    "image": "alpine",
    "memory": 256,
    "vcpus": 1,
    "jobRoleArn":"${aws_iam_role.ecs_task_role.arn}"
}
CONTAINER_PROPERTIES
}

output batch_compute_environment_arn {
  value = "${aws_batch_compute_environment.compute_environment.arn}"
}

output batch_job_queue_high_priority_arn {
  value = "${aws_batch_job_queue.high_priority.arn}"
}

output batch_job_queue_low_priority_arn {
  value = "${aws_batch_job_queue.low_priority.arn}"
}

output batch_iam_ecs_task_role_arn {
  value = "${aws_iam_role.ecs_task_role.arn}"
}

output batch_job_definition_base_arn {
  value = "${aws_batch_job_definition.base.arn}"
}