variable "project" {}
variable "env" {}
variable "ver" {}

locals {
  cluster_name = "${var.project}-${var.env}-${var.ver}"
}
